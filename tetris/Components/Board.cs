﻿
namespace tetrisCompnents
{
	internal class Board
	{
		private const int length = 11;
		private const int height = 22;
		internal int GetLength()
		{
			return length;
		}
		internal int GetHeight()
		{
			return height;
		}
		private readonly char[,] board = new char[height, length]
		{
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z',' ',' ',' ',' ',' ',' ',' ',' ',' ','Z'},
			{'Z','Z','Z','Z','Z','Z','Z','Z','Z','Z','Z'}
		};

		internal char GetBoardCell(int posY, int posX)
		{
			try
			{
				return board[posY, posX];
			}
			catch (System.IndexOutOfRangeException ex)
			{
				System.Console.WriteLine(ex);
				return 'X';
			}
		}
		internal void Fill(int posY, int posX, char cell)
		{
			if (cell != ' ')
			{
				try
				{
					board[posY, posX] = 'X';
				}
				catch (System.IndexOutOfRangeException ex)
				{
					System.Console.WriteLine(ex);
				}
			}
		}
	}
}
