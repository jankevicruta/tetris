﻿using System;

namespace tetrisCompnents
{
	internal partial class Block
	{
		private readonly Blocks blocks = new Blocks();
		private const int blockNum = 4, rotateNum = 4;
		private const int blockSize = 3;
		internal static int BlockSize => blockSize;
		private int rotation = 1;
		private readonly int type;

		internal int X { get; set; }
		internal int Y { get; set; }

		internal char[,] block = new char[blockSize, blockSize];

		internal Block(int posY, int posX)
		{
			Random r = new Random();
			type = r.Next(0, blockNum - 1);
			SetBlock();
			X = posX;
			Y = posY;
		}
		internal void Rotate()
		{
			if (rotation < 3)
			{
				rotation++;
			}
			else
			{
				rotation = 0;
			}
			SetBlock();
		}
		internal char GetBlockCell (int cellPosY, int cellPosX)
		{
			int tmpCellposX, tmpCellPosY;
			if (X > cellPosX)
			{
				tmpCellposX = 0;
			}
			else if (X == cellPosX)
			{
				tmpCellposX = 1;
			}
			else
			{
				tmpCellposX = 2;
			}

			if (Y > cellPosY)
			{
				tmpCellPosY = 0;
			}
			else if (Y == cellPosY)
			{
				tmpCellPosY = 1;
			}
			else
			{
				tmpCellPosY = 2;
			}
			return block[tmpCellPosY, tmpCellposX];
		}
		internal char GetCellByIndex(int cellPosY, int cellPosX)
		{
			return block[cellPosY, cellPosX];
		}
		private void SetBlock()
		{
			for (int i = 0; i < blockSize; i++)
			{
				for (int j = 0; j < blockSize; j++)
				{
					block[i, j] = blocks.blocksArr[type, rotation, i, j];
				}
			}
		}
	}
}
