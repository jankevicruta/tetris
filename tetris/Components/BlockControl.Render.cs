﻿using System;

namespace tetrisCompnents
{
	public partial class GameControler
	{
		private class Render
		{
			Block block;
			Board board;

			public Render(Block block, Board board)
			{
				this.block = block;
				this.board = board;
			}
			public void RenderGame()
			{
				Console.Clear();
				for (int i = 0; i < board.GetHeight(); i++)
				{
					for (int j = 0; j < board.GetLength(); j++)
					{
						if (i == block.Y - 1 || i == block.Y || i == block.Y + 1)
						{
							if ((j == block.X - 1 || j == block.X || j == block.X + 1) && block.GetBlockCell(i, j) != ' ')
							{
								Console.Write(block.GetBlockCell(i, j));
							}
							else
							{
								Console.Write(board.GetBoardCell(i, j));
							}
						}
						else
						{
							Console.Write(board.GetBoardCell(i, j));
						}
					}
					Console.WriteLine();
				}
			}
		}
	}
}
