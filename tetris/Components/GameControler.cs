﻿using System;
using System.Timers;

namespace tetrisCompnents
{
	public partial class GameControler
	{
		private Board board;
		private Block block;
		private Timer Playtimer;
		private Render render;

		private bool running = true;
		private ConsoleKeyInfo key;

		public GameControler()
		{
			board = new Board();
			int posX = board.GetLength() / 2;
			int posY = 0;
			block = new Block(posY, posX);
			render = new Render(block, board);
		}
		public void Play()
		{
			SetPlayTimer();
			while (running)
			{
				if (Console.KeyAvailable)
				{
					key = Console.ReadKey(true);
					Move(key);
					render.RenderGame();
				}
			}
		}

		private void Move(ConsoleKeyInfo key)
		{
			switch (key.Key)
			{
				case ConsoleKey.UpArrow:
					block.Rotate();
				break;
				case ConsoleKey.RightArrow:
					if (CanMove(block.Y, block.X + 1) == PlayStates.CAN)
					{
						block.X++;
					}
				break;
				case ConsoleKey.LeftArrow:
					if (CanMove(block.Y, block.X - 1) == PlayStates.CAN)
					{
						block.X--;
					}
				break;
				case ConsoleKey.Escape:
					Environment.Exit(0);
				break;
			}
		}
		private PlayStates CanMove(int currPosY, int currPosX)
		{
			for (int blockYIndex = 0; blockYIndex < Block.BlockSize; blockYIndex++)
			{
				for (int blockXIndex = 0; blockXIndex < Block.BlockSize; blockXIndex++)
				{
					int cellPosX = -1, cellPosY = -1;
					cellPosX += currPosX + blockXIndex;
					cellPosY += currPosY + blockYIndex;
					if ((cellPosX < 1 || cellPosX >= board.GetLength() - 1) && block.GetCellByIndex(blockYIndex, blockXIndex) != ' ')
					{
						return PlayStates.BORDER;
					}
					if ((cellPosY >= board.GetHeight()-1 || board.GetBoardCell(cellPosY, cellPosX) != ' ') && block.GetCellByIndex(blockYIndex, blockXIndex) != ' ')
					{
						Place(currPosY-1, currPosX);
						if (cellPosY <= 0)
							Environment.Exit(0);
						return PlayStates.BLOCK;
					}
				}
			}
			return PlayStates.CAN;
		}
		private void Place(int currPosY, int currPosX)
		{
			for (int blockYIndex = 0; blockYIndex < Block.BlockSize; blockYIndex++)
			{
				for (int blockXIndex = 0; blockXIndex < Block.BlockSize; blockXIndex++)
				{
					int cellPosX = -1, cellPosY = -1;
					cellPosX += currPosX + blockXIndex;
					cellPosY += currPosY + blockYIndex;
					board.Fill(cellPosY, cellPosX, block.GetCellByIndex(blockYIndex, blockXIndex));
				}
			}
			block = new Block(0, board.GetLength() / 2);
			render = new Render(block, board);
		}

		private void SetPlayTimer()
		{
			Playtimer = new Timer(500);
			Playtimer.Elapsed += new ElapsedEventHandler(OnPlayTimeEvent);
			Playtimer.Enabled = true;
		}
		private void OnPlayTimeEvent(Object source, ElapsedEventArgs e)
		{
			MoveDown();
			Draw();
		}
		private void Draw()
		{
			render.RenderGame();
		}
		private void MoveDown()
		{
			int tmpY = block.Y;
			if (CanMove(tmpY + 1, block.X) == PlayStates.CAN)
			{
				++block.Y;
			}
		}
	}
}
